const express = require("express");
const mongoose = require("mongoose");
const bodyparser = require("body-parser");
const cookieParser = require("cookie-parser");

const app = express();
// app use
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.use(cookieParser());

app.get("/login", function (req, res) {
  console.log("req : ", req);
  res.status(200).send(`Welcome to login , sign-up api`);
});

// listening port
//const PORT = process.env.PORT || 3000;
const PORT =  3000;
app.listen(PORT, () => {
  console.log(`app is live at ${PORT}`);
});
